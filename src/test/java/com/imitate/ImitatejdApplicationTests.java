package com.imitate;

import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

@SpringBootTest
class ImitatejdApplicationTests {

    @Autowired
    @Qualifier("restHighLevelClient")
    private RestHighLevelClient client;

    @Test
    void showHealth() throws IOException {
        GetIndexRequest indexRequest = new GetIndexRequest("jd_goods");
        boolean exists = client.indices().exists(indexRequest, RequestOptions.DEFAULT);
        System.out.println("是否存在：" + exists);
    }

    @Test
    void createIndex() throws IOException {
        CreateIndexRequest indexRequest = new CreateIndexRequest("jd_goods");
        CreateIndexResponse createIndexResponse = client.indices().create(indexRequest, RequestOptions.DEFAULT);
        System.out.println(createIndexResponse);
    }

    @Test
    void deleteIndex() throws IOException {
        DeleteIndexRequest indexRequest = new DeleteIndexRequest("jd_goods");
        AcknowledgedResponse deleteResponse = client.indices().delete(indexRequest, RequestOptions.DEFAULT);
        System.out.println(deleteResponse.isAcknowledged());
    }

}
