package com.imitate.service;

import com.alibaba.fastjson.JSON;
import com.imitate.pojo.Content;
import com.imitate.utils.HtmlParseUrl;
import com.sun.org.apache.xerces.internal.xs.datatypes.ObjectList;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.data.elasticsearch.annotations.Highlight;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class ScrwingService {

    @Autowired
    private RestHighLevelClient restHighLevelClient;

    public boolean parseContent(String keyword) throws IOException {
        List<Content> contents = HtmlParseUrl.parseUrl(keyword);
        BulkRequest bulkRequest = new BulkRequest();
        bulkRequest.timeout("2m");
        for (int i = 0; i < contents.size(); i++) {
            bulkRequest.add(new IndexRequest("jd_goods").source(JSON.toJSONString(contents.get(i)), XContentType.JSON));
        }
        BulkResponse bulk = restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
        return !bulk.hasFailures();
    }

    public List<Map<String, Object>> searchGoods(String keyword, int pageSize, int pageNo) throws IOException {
        // 设置查询的索引
        SearchRequest searchRequest = new SearchRequest("jd_goods");
        // 构建builder
        SearchSourceBuilder builder = new SearchSourceBuilder();
        builder.query(QueryBuilders.termQuery("title", keyword));
        builder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        // 设置高亮搜索，如果不设置前缀和后缀默认为<em></em>
        HighlightBuilder highlightBuilder = new HighlightBuilder();
        // 设置高亮字段
        highlightBuilder.field("title");
        // 设置只需要高亮一个，其他不进行高亮显示，此功能存在问题，设置为false仍然所有的
        // 搜索字段都为高亮显示，没有找到问题所在
        highlightBuilder.requireFieldMatch(false);
        // 设置高亮的前缀后缀
        highlightBuilder.preTags("<span style='color: red'>");
        highlightBuilder.postTags("</span>");
        builder.highlighter(highlightBuilder);

        // 设置分页查询
        builder.from(pageNo);
        builder.size(pageSize);

        searchRequest.source(builder);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        SearchHit[] hits = searchResponse.getHits().getHits();
        List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
        for (SearchHit hit : hits) {
            // 解析高亮字段
            Map<String, HighlightField> highlightFields = hit.getHighlightFields();
            HighlightField title = highlightFields.get("title");
            Map<String, Object> hitSourceMap = hit.getSourceAsMap();
            // 替换Map中的title为查询出的高亮字段
            if (title != null) {
                Text[] fragments = title.getFragments();
                String newTitle = "";
                for (Text fragment : fragments) {
                    newTitle += fragment;
                }
                hitSourceMap.put("title", newTitle);
            }
            mapList.add(hitSourceMap);
        }
        return mapList;
    }
}
