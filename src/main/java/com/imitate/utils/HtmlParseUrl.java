package com.imitate.utils;

import com.imitate.pojo.Content;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class HtmlParseUrl {

    public static void main(String[] args) throws IOException {
        System.out.println("333");
        parseUrl("java");
    }

    public static List<Content> parseUrl(String keyword) throws IOException {
        String url = "https://search.jd.com/Search?keyword=" + keyword;
        Document document = Jsoup.parse(new URL(url), 30000);
        Element element = document.getElementById("J_goodsList");
        Elements lis = element.getElementsByTag("li");
//        System.out.println(lis.outerHtml());
        List<Content> list = new ArrayList<Content>();
        for (Element li : lis) {
            // source-data-lazy-img
            // source-data-lazy-img
            String img = li.getElementsByTag("img").eq(0).attr("src");
            System.out.println(img);
            String price = li.getElementsByClass("p-price").eq(0).text();
            String title = li.getElementsByClass("p-name").eq(0).text();
            Content content = new Content();
            content.setImg(img);
            content.setPrice(price);
            content.setTitle(title);
            list.add(content);
        }
        return list;
    }
}
