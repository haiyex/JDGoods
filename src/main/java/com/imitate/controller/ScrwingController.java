package com.imitate.controller;

import com.imitate.service.ScrwingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Controller
public class ScrwingController {

    @Autowired
    private ScrwingService scrwingService;

    @RequestMapping({"/", "/index"})
    public String index() {
        return "index";
    }

    @RequestMapping("/scrw/{keyword}")
    @ResponseBody
    public boolean Scrw(@PathVariable("keyword") String keyword) {
        try {
            return scrwingService.parseContent(keyword);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    @RequestMapping("/search/{keyword}/{pageNo}/{pageSize}")
    @ResponseBody
    public List<Map<String, Object>> SearchGoods(@PathVariable("keyword") String keyword,
                                                 @PathVariable("pageSize") int pageSize,
                                                 @PathVariable("pageNo") int pageNo) {

        List<Map<String, Object>> mapList = null;
        try {
            mapList = scrwingService.searchGoods(keyword, pageSize, pageNo);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mapList;
    }

}
