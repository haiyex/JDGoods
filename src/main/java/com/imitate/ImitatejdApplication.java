package com.imitate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImitatejdApplication {

    public static void main(String[] args) {
        SpringApplication.run(ImitatejdApplication.class, args);
    }

}
